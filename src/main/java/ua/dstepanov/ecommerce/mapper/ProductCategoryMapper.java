package ua.dstepanov.ecommerce.mapper;

import org.mapstruct.Mapper;
import ua.dstepanov.ecommerce.config.MapperConfig;
import ua.dstepanov.ecommerce.dto.ProductCategoryDto;
import ua.dstepanov.ecommerce.entity.ProductCategory;

@Mapper(config = MapperConfig.class)
public interface ProductCategoryMapper {
    ProductCategoryDto productCategoryToProductCategoryDto(ProductCategory productCategory);
}
