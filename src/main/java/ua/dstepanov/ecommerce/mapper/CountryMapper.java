package ua.dstepanov.ecommerce.mapper;

import org.mapstruct.Mapper;
import ua.dstepanov.ecommerce.config.MapperConfig;
import ua.dstepanov.ecommerce.dto.CountryDto;
import ua.dstepanov.ecommerce.entity.Country;

@Mapper(config = MapperConfig.class)
public interface CountryMapper {
    CountryDto countryToCountryDto(Country country);
}
