package ua.dstepanov.ecommerce.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ua.dstepanov.ecommerce.config.MapperConfig;
import ua.dstepanov.ecommerce.dto.ProductDto;
import ua.dstepanov.ecommerce.entity.Product;

@Mapper(config = MapperConfig.class)
public interface ProductMapper {
    @Mapping(source = "category.id", target = "categoryId")
    ProductDto productToProductDto(Product product);
}
