package ua.dstepanov.ecommerce.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ua.dstepanov.ecommerce.config.MapperConfig;
import ua.dstepanov.ecommerce.dto.StateDto;
import ua.dstepanov.ecommerce.entity.State;

@Mapper(config = MapperConfig.class)
public interface StateMapper {
    @Mapping(source = "country.name", target = "countryName")
    @Mapping(source = "country.code", target = "countryCode")
    StateDto stateToStateDto(State state);
}
