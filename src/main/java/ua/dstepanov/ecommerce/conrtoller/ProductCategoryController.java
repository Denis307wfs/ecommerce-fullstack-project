package ua.dstepanov.ecommerce.conrtoller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.dstepanov.ecommerce.dto.ProductCategoryDto;
import ua.dstepanov.ecommerce.service.ProductCategoryService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/categories")
@CrossOrigin("http://localhost:4200")
public class ProductCategoryController {
    private final ProductCategoryService productCategoryService;

    @GetMapping
    public Page<ProductCategoryDto> getAll(Pageable pageable) {
        return productCategoryService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public ProductCategoryDto getById(@PathVariable Long id) {
        return productCategoryService.getById(id);
    }
}
