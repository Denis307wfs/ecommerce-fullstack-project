package ua.dstepanov.ecommerce.conrtoller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.dstepanov.ecommerce.dto.ProductDto;
import ua.dstepanov.ecommerce.dto.ProductSpecDto;
import ua.dstepanov.ecommerce.service.ProductService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/products")
@CrossOrigin("http://localhost:4200")
public class ProductController {
    private final ProductService productService;

    @GetMapping
    public Page<ProductDto> getAll(ProductSpecDto productSpecDto, Pageable pageable) {
        return productService.getAll(productSpecDto, pageable);
    }

    @GetMapping("/{id}")
    public ProductDto getById(@PathVariable Long id) {
        return productService.getById(id);
    }

    @GetMapping("/category/{categoryId}")
    public Page<ProductDto> getByCategoryId(@PathVariable Long categoryId, Pageable pageable) {
        return productService.getByCategoryId(categoryId, pageable);
    }
}
