package ua.dstepanov.ecommerce.conrtoller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.dstepanov.ecommerce.dto.CountryDto;
import ua.dstepanov.ecommerce.service.CountryService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/countries")
@CrossOrigin("http://localhost:4200")
public class CountryController {
    private final CountryService countryService;

    @GetMapping
    public Page<CountryDto> findAll(Pageable pageable) {
        return countryService.findAll(pageable);
    }
}
