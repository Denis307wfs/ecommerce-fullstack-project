package ua.dstepanov.ecommerce.conrtoller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.dstepanov.ecommerce.dto.StateCountryCodeDto;
import ua.dstepanov.ecommerce.dto.StateDto;
import ua.dstepanov.ecommerce.service.StateService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/states")
@CrossOrigin("http://localhost:4200")
public class StateController {
    private final StateService stateService;

    @GetMapping
    public Page<StateDto> getAll(StateCountryCodeDto countryCodeDto, Pageable pageable) {
        return stateService.getAll(countryCodeDto, pageable);
    }
}
