package ua.dstepanov.ecommerce.dto;

import java.math.BigDecimal;
import java.util.Date;

public record ProductDto(Long id, int categoryId, String sku, String name, String description,
                         BigDecimal unitPrice, String imageUrl, boolean active, int unitsInStock,
                         Date dateCreated, Date lastUpdated) {
}
