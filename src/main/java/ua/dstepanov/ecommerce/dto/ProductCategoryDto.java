package ua.dstepanov.ecommerce.dto;

public record ProductCategoryDto(Long id, String categoryName) {
}
