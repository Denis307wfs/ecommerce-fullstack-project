package ua.dstepanov.ecommerce.dto;

public record CountryDto(Long id, String code, String name) {
}
