package ua.dstepanov.ecommerce.dto;

public record StateCountryCodeDto(String[] countryCode) {
}
