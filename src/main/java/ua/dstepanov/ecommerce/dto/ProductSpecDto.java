package ua.dstepanov.ecommerce.dto;

public record ProductSpecDto(String[] name, String[] unitPrice) {
}
