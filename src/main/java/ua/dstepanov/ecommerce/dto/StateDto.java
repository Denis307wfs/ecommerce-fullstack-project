package ua.dstepanov.ecommerce.dto;

public record StateDto(Long id,
                       String name,
                       String countryName,
                       String countryCode) {
}
