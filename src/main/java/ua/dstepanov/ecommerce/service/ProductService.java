package ua.dstepanov.ecommerce.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ua.dstepanov.ecommerce.dto.ProductDto;
import ua.dstepanov.ecommerce.dto.ProductSpecDto;
import ua.dstepanov.ecommerce.entity.Product;
import ua.dstepanov.ecommerce.mapper.ProductMapper;
import ua.dstepanov.ecommerce.repository.ProductRepository;
import ua.dstepanov.ecommerce.repository.spec.impl.ProductSpecBuilder;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final ProductSpecBuilder productSpecBuilder;


    public Page<ProductDto> getAll(ProductSpecDto productSpecDto, Pageable pageable) {
        Specification<Product> specification = productSpecBuilder.build(productSpecDto);
        return productRepository.findAll(specification, pageable)
                .map(productMapper::productToProductDto);
    }

    public ProductDto getById(Long id) {
        Product product = productRepository.findById(id).orElseThrow();
        return productMapper.productToProductDto(product);
    }

    public Page<ProductDto> getByCategoryId(Long categoryId, Pageable pageable) {
        return productRepository.findProductsByCategoryId(categoryId, pageable).map(
                productMapper::productToProductDto);
    }
}
