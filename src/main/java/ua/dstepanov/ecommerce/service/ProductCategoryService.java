package ua.dstepanov.ecommerce.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.dstepanov.ecommerce.dto.ProductCategoryDto;
import ua.dstepanov.ecommerce.mapper.ProductCategoryMapper;
import ua.dstepanov.ecommerce.repository.ProductCategoryRepository;

@Service
@RequiredArgsConstructor
public class ProductCategoryService {
    private final ProductCategoryRepository productCategoryRepository;
    private final ProductCategoryMapper productCategoryMapper;

    public Page<ProductCategoryDto> getAll(Pageable pageable) {
        return productCategoryRepository.findAll(pageable).map(
                productCategoryMapper::productCategoryToProductCategoryDto);
    }

    public ProductCategoryDto getById(Long id) {
        return productCategoryRepository.findById(id).map(
                productCategoryMapper::productCategoryToProductCategoryDto
        ).orElseThrow();
    }
}
