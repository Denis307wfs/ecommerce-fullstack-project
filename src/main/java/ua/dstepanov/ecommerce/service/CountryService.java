package ua.dstepanov.ecommerce.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.dstepanov.ecommerce.dto.CountryDto;
import ua.dstepanov.ecommerce.mapper.CountryMapper;
import ua.dstepanov.ecommerce.repository.CountryRepository;

@Service
@RequiredArgsConstructor
public class CountryService {
    private final CountryRepository countryRepository;
    private final CountryMapper countryMapper;


    public Page<CountryDto> findAll(Pageable pageable) {
        return countryRepository.findAll(pageable)
                .map(countryMapper::countryToCountryDto);
    }
}
