package ua.dstepanov.ecommerce.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ua.dstepanov.ecommerce.dto.StateCountryCodeDto;
import ua.dstepanov.ecommerce.dto.StateDto;
import ua.dstepanov.ecommerce.entity.State;
import ua.dstepanov.ecommerce.mapper.StateMapper;
import ua.dstepanov.ecommerce.repository.StateRepository;
import ua.dstepanov.ecommerce.repository.spec.impl.StateSpecBuilder;

@Service
@RequiredArgsConstructor
public class StateService {
    private final StateRepository stateRepository;
    private final StateMapper stateMapper;
    private final StateSpecBuilder builder;

    public Page<StateDto> getAll(StateCountryCodeDto countryCodeDto, Pageable pageable) {
        Specification<State> specification = builder.build(countryCodeDto);
        return stateRepository.findAll(specification, pageable)
                .map(stateMapper::stateToStateDto);
    }
}
