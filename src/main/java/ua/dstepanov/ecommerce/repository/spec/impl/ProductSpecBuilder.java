package ua.dstepanov.ecommerce.repository.spec.impl;

import java.lang.reflect.Field;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import ua.dstepanov.ecommerce.entity.Product;
import ua.dstepanov.ecommerce.repository.spec.SpecBuilder;
import ua.dstepanov.ecommerce.repository.spec.SpecProvider;

@Component
@RequiredArgsConstructor
public class ProductSpecBuilder implements SpecBuilder<Product> {
    private final ProductProviderManager productProviderManager;

    @Override
    public Specification<Product> build(Record params) {
        Specification<Product> specification = Specification.where(null);
        Field[] fields = params.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            SpecProvider<Product> provider = productProviderManager.getProvider(field.getName());
            try {
                String[] fieldValue = (String[]) field.get(params);
                if (fieldValue != null) {
                    specification = specification.and(provider.getSpecification(fieldValue));
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return specification;
    }
}
