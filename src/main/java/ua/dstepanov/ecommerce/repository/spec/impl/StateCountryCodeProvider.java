package ua.dstepanov.ecommerce.repository.spec.impl;

import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import ua.dstepanov.ecommerce.entity.Country;
import ua.dstepanov.ecommerce.entity.State;
import ua.dstepanov.ecommerce.repository.spec.SpecProvider;

@Component
public class StateCountryCodeProvider implements SpecProvider<State> {

    @Override
    public String getName() {
        return "countryCode";
    }

    @Override
    public Specification<State> getSpecification(String[] params) {
        return ((root, query, criteriaBuilder) -> {
            query.distinct(true);
            Join<State, Country> countryJoin = root.join("country");
            Expression<String> countryCodeExp = countryJoin.get("code");

            Predicate predicate = criteriaBuilder.disjunction();
            for (String param : params) {
                predicate = criteriaBuilder.or(predicate,
                        criteriaBuilder.like(criteriaBuilder.upper(countryCodeExp),
                        param.toUpperCase()));
            }
            return predicate;

            // or with Stream API
            /*return criteriaBuilder.or(
                    Arrays.stream(params)
                            .map(code -> criteriaBuilder
                                    .equal(criteriaBuilder
                                                    .upper(countryCodeExp),
                                            code.toUpperCase()))
                            .toArray(Predicate[]::new)
            );*/
        });
    }
}
