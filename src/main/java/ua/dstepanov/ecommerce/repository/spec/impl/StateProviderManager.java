package ua.dstepanov.ecommerce.repository.spec.impl;

import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ua.dstepanov.ecommerce.entity.State;
import ua.dstepanov.ecommerce.repository.spec.SpecProvider;
import ua.dstepanov.ecommerce.repository.spec.SpecProviderManager;

@Component
@RequiredArgsConstructor
public class StateProviderManager implements SpecProviderManager<State> {
    private final Set<SpecProvider<State>> stateSpecs;

    @Override
    public SpecProvider<State> getProvider(String key) {
        return stateSpecs.stream()
                .filter(spec -> spec.getName().equals(key))
                .findFirst()
                .orElseThrow();
    }
}
