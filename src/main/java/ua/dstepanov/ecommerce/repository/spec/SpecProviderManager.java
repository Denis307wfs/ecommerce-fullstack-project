package ua.dstepanov.ecommerce.repository.spec;

public interface SpecProviderManager<T> {
    SpecProvider<T> getProvider(String key);
}
