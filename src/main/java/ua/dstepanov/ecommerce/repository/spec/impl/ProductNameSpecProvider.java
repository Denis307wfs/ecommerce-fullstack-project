package ua.dstepanov.ecommerce.repository.spec.impl;

import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import ua.dstepanov.ecommerce.entity.Product;
import ua.dstepanov.ecommerce.repository.spec.SpecProvider;

@Component
public class ProductNameSpecProvider implements SpecProvider<Product> {
    @Override
    public String getName() {
        return "name";
    }

    @Override
    public Specification<Product> getSpecification(String[] params) {
        return (root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.conjunction();
            for (String param : params) {
                predicate = criteriaBuilder.and(predicate,
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
                                "%"+param.toLowerCase()+"%"));
            }
            return predicate;
        };
    }
}
