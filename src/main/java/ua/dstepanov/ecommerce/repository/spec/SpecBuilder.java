package ua.dstepanov.ecommerce.repository.spec;

import org.springframework.data.jpa.domain.Specification;

public interface SpecBuilder<T> {
    Specification<T> build(Record params);
}
