package ua.dstepanov.ecommerce.repository.spec.impl;

import java.math.BigDecimal;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import ua.dstepanov.ecommerce.entity.Product;
import ua.dstepanov.ecommerce.repository.spec.SpecProvider;

@Component
public class ProductPriceSpecProvider implements SpecProvider<Product> {
    @Override
    public String getName() {
        return "unitPrice";
    }

    @Override
    public Specification<Product> getSpecification(String[] params) {
        BigDecimal minPrice = new BigDecimal(params[0]);
        BigDecimal maxPrice = new BigDecimal(params[1]);
        return (root, query, criteriaBuilder) ->
            criteriaBuilder.between(root.get("unitPrice"), minPrice, maxPrice);
    }
}
