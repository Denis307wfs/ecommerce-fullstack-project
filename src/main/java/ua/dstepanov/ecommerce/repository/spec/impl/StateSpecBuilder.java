package ua.dstepanov.ecommerce.repository.spec.impl;

import java.lang.reflect.Field;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import ua.dstepanov.ecommerce.entity.State;
import ua.dstepanov.ecommerce.repository.spec.SpecBuilder;
import ua.dstepanov.ecommerce.repository.spec.SpecProvider;

@Component
@RequiredArgsConstructor
public class StateSpecBuilder implements SpecBuilder<State> {
    private final StateProviderManager stateProviderManager;

    @Override
    public Specification<State> build(Record params) {
        Specification<State> specification = Specification.where(null);
        Field[] fields = params.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            SpecProvider<State> provider = stateProviderManager.getProvider(field.getName());
            try {
                String[] fieldValue = (String[]) field.get(params);
                if (fieldValue != null) {
                    specification = specification.and(provider.getSpecification(fieldValue));
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return specification;
    }
}
