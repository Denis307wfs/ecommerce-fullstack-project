package ua.dstepanov.ecommerce.repository.spec.impl;

import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ua.dstepanov.ecommerce.entity.Product;
import ua.dstepanov.ecommerce.repository.spec.SpecProvider;
import ua.dstepanov.ecommerce.repository.spec.SpecProviderManager;

@Component
@RequiredArgsConstructor
public class ProductProviderManager implements SpecProviderManager<Product> {
    private final Set<SpecProvider<Product>> productSpecs;

    @Override
    public SpecProvider<Product> getProvider(String key) {
        return productSpecs.stream()
                .filter(spec -> spec.getName().equals(key))
                .findFirst()
                .orElseThrow();
    }
}
