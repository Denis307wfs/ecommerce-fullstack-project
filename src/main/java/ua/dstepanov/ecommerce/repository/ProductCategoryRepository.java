package ua.dstepanov.ecommerce.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import ua.dstepanov.ecommerce.entity.ProductCategory;

//@RepositoryRestResource(collectionResourceRel = "productCategory", path = "product-category")
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {
    @NonNull
    Page<ProductCategory> findAll(@NonNull Pageable pageable);
}
