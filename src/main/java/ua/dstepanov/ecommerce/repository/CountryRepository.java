package ua.dstepanov.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.dstepanov.ecommerce.entity.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
}
